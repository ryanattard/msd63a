﻿using Common;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class CategoriesRepository: ConnectionClass
    {
        public CategoriesRepository():base()
        { }

        public List<Category> GetCategories()
        {
            NpgsqlCommand cmd = new NpgsqlCommand("Select Id, Title From Categories",
                MyConnection);

            List<Category> myResult = new List<Category>();

            using (NpgsqlDataReader myReader = cmd.ExecuteReader())
            {
                while (myReader.Read())
                {
                    myResult.Add(
                        new Category()
                        {
                            Id = myReader.GetInt32(0),
                            Title = myReader.GetString(1) 
                        });
                }
            }

            return myResult;
        }
    }
}
