﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Npgsql;
using System.Web;
using System.Configuration;
using System.Web.Configuration;

namespace DataAccess
{
    public class ConnectionClass
    {
        public NpgsqlConnection MyConnection { get; set; }

        public ConnectionClass()
        {
            var str = WebConfigurationManager.ConnectionStrings["postgresConnection"].ConnectionString;
            MyConnection = new NpgsqlConnection(str);
        }
       
    }
}
