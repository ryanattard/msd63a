﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using Common;

namespace DataAccess
{
    public class ItemsRepository: ConnectionClass
    {
        public ItemsRepository() : base() { }


        public List<Item> GetItems()
        {
            //Select * From Items

            NpgsqlCommand cmd = new NpgsqlCommand("Select Id, Name, Price, Category_Fk, imagepath From Items", MyConnection);

            List<Item> myResult = new List<Item>();

            using (NpgsqlDataReader myReader = cmd.ExecuteReader())
            {
                while(myReader.Read())
                {
                    myResult.Add(
                        new Item()
                        {
                            Id = myReader.GetInt32(0),
                            Name = myReader.GetString(1),
                            Price = myReader.GetDecimal(2),
                            Category_Fk = myReader.GetInt32(3),
                            ImagePath = string.IsNullOrEmpty(myReader[4].ToString())? "" : myReader.GetString(4)
                        });
                }
            }

            return myResult;
        }


        public List<Item> GetItems(int category)
        {
            //Select * From Items

            NpgsqlCommand cmd = new NpgsqlCommand("Select Id, Name, Price, Category_Fk From Items where category_fk = @category" 
                , MyConnection);
            cmd.Parameters.AddWithValue("@category", category);


            List<Item> myResult = new List<Item>();

            using (NpgsqlDataReader myReader = cmd.ExecuteReader())
            {
                while (myReader.Read())
                {
                    myResult.Add(
                        new Item()
                        {
                            Id = myReader.GetInt32(0),
                            Name = myReader.GetString(1),
                            Price = myReader.GetDecimal(2),
                            Category_Fk = myReader.GetInt32(3)
                        });
                }
            }

            return myResult;
        }



        public void AddItem(Item i)
        {
            string sql = "INSERT into items ( name, price, category_fk, imagepath) values(@name, @price, @category, @imagepath)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@name", i.Name);
            cmd.Parameters.AddWithValue("@price", i.Price);
            cmd.Parameters.AddWithValue("@category", i.Category_Fk);
            cmd.Parameters.AddWithValue("@imagepath", i.ImagePath);
            cmd.ExecuteNonQuery();

        }



        public void DeleteItem(int id)
        {
            string sql = "delete from items where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            cmd.ExecuteNonQuery();
        }



    }



}
