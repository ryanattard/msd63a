﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.Diagnostics.AspNet;


namespace DataAccess
{
     public class LoggingRepository
    {
        public static void ReportException(Exception ex)
        {
            var logger = GoogleExceptionLogger.Create("progforcloud63at", "msd63aClassDemo", "1");
            logger.Log(ex);
        }
    }
}
