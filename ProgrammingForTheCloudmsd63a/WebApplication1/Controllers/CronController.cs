﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class CronController : Controller
    {
        // GET: Cron

        [ApiKeyAuthorize]
        public ActionResult RunHourlyMethod()
        {
           if(HttpContext.Application["counter"] != null)
            {
                int counter = Convert.ToInt32(HttpContext.Application["counter"]);
                counter++;
                HttpContext.Application["counter"] = counter;
            }
            return Content("done");
        }

        [ApiKeyAuthorize]
        public ActionResult RunDailyMethod()
        {
            RedisManagement rm = new RedisManagement();
            ItemsRepository ir = new ItemsRepository();
            ir.MyConnection.Open();

            var listOfItems = ir.GetItems();

            ir.MyConnection.Close();

           var result = rm.SaveInRedis(listOfItems);
            if (result)
                return Content("Done updated");
            else return Content("not updated. items are still the same");
        }


    }

    public class ApiKeyAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                //if (String.IsNullOrEmpty(httpContext.Request.Form["api-key"]) == true) to read it from message body
                    if (String.IsNullOrEmpty(httpContext.Request.Headers["api-key"]) == true)
                    return false;
                else
                {
                    string apiKeyEncoded = httpContext.Request.Headers["api-key"];
                    byte[] apiKeyDecoded = Convert.FromBase64String(apiKeyEncoded);
                    Guid apiKey = new Guid(Encoding.ASCII.GetString(apiKeyDecoded));

                    if (apiKey == new Guid("689BEC23-3A93-4C37-9157-CDF48A39A72A"))
                    {
                        return true;
                    }

                    else return false;

                }
            }
            catch
            {
                return false;
            }
        }

    }



}