﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Npgsql;
using Common;
using DataAccess;
using Google.Cloud.Storage.V1;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ItemsController : Controller
    {
        // GET: Items

            [Authorize]
        public ActionResult Index()
        {

            PubSubManagement p = new PubSubManagement();
            var t = p.CreateOrRetrieveTopic("MessageTopic");
            p.PublishMessageToTopic("Loading Index page", t);





            ItemsRepository ir = new ItemsRepository();

            if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                ir.MyConnection.Open();

            RedisManagement rm = new RedisManagement();

            var list = rm.LoadFromRedis();
            if (list == null) ir.GetItems();

            if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                ir.MyConnection.Close();

            return View(list);
        }


        public ActionResult Search(int category)
        {
            ItemsRepository ir = new ItemsRepository();

            if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                ir.MyConnection.Open();

            var list = ir.GetItems(category);

            if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                ir.MyConnection.Close();

            return View("Index",list);
        }


        public List<Category> GetCategories()
        {
            CategoriesRepository cr = new CategoriesRepository();

            if (cr.MyConnection.State == System.Data.ConnectionState.Closed)
                cr.MyConnection.Open();

            var list = cr.GetCategories();

            if (cr.MyConnection.State == System.Data.ConnectionState.Open)
                cr.MyConnection.Close(); ;

            return list;
        }


        [HttpGet][Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize] //checks whether the user is anonymous or not
        public ActionResult Create(Item i, HttpPostedFileBase file)
        {
            try
            {
                //install NuGet package Google.Cloud.Storage.V1

                ItemsRepository ir = new ItemsRepository();

                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                //code to save the file on the cloud bucket

                var myObjUploader = StorageClient.Create();

                var newName = Guid.NewGuid() + System.IO.Path.GetExtension(file.FileName);

               var myImage=  myObjUploader.UploadObject(new Google.Apis.Storage.v1.Data.Object()
               { Bucket = "programmingforcloudmsd63at", Name=newName, ContentType=file.ContentType}, file.InputStream,
                     new UploadObjectOptions() { PredefinedAcl = PredefinedObjectAcl.PublicRead });

               //var myImage = myObjUploader.UploadObject("programmingforcloudmsd63at", newName, file.ContentType
               //     , file.InputStream, new UploadObjectOptions() { PredefinedAcl = PredefinedObjectAcl.PublicRead });

                i.ImagePath = myImage.MediaLink;
                ir.AddItem(i);

                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();

                ViewBag.Success = "Item created successfully";
            }
            catch (Exception ex)
            {
                //log errors in the cloud
                LoggingRepository.ReportException(ex);
                ViewBag.Error = "Failed to create a new item";
            }
            return View();
        }


        public ActionResult TestRaiseException()
        {
            try
            {
                throw new Exception("Raising a test exception message to test feature");
            }catch (Exception ex)
            {
                LoggingRepository.ReportException(ex);
            }
            return Content("done");
        }


        public ActionResult Delete(int id)
        {
            List<Item> items = new List<Item>();
            ItemsRepository ir = new ItemsRepository();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                ir.DeleteItem(id);
                items = ir.GetItems();

                ViewBag.Success = "Item deleted successfully";
            }
            catch (Exception ex)
            {
                //log errors in the cloud
                ViewBag.Error = "Failed to delete the item";
            }
            finally
            { if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
            return View("Index", items );
        }





    }
}