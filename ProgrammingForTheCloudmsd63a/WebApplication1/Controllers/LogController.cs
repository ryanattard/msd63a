﻿using Google.Cloud.Datastore.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class LogController : Controller
    {
        // GET: Log
        public ActionResult Index()
        {
            var dbStore = DatastoreDb.Create("progforcloud63at");
            var table = dbStore.CreateKeyFactory("Log");

            Query q = new Query("Log");
            List<UserLog> logs = new List<UserLog>();

            var result = dbStore.RunQuery(q);

            foreach(Entity e in result.Entities)
            {
                UserLog l = new UserLog()
                {
                    Email = e["email"].StringValue,
                    LoggedIn = e["loggedIn"].TimestampValue.ToDateTime()
                };

                try
                {
                    l.LoggedOut = e["loggedOut"].TimestampValue.ToDateTime();
                }
                catch
                {
                    //meaning that loggedOut is still null...i.e. user is still logged in
                }
                logs.Add(l);
            }


            return View(logs);
        }
    }
}