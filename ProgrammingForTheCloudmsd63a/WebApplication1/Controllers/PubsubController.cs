﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PubsubController : Controller
    {
        // GET: Pubsub
        public ActionResult Index()
        {
            PubSubManagement psm = new PubSubManagement();
            var myQueue = psm.CreateOrRetrieveTopic("MessageQueueMSD63a");

           psm.PublishMessageToTopic("hello world", myQueue);

            string msgRead = psm.PullMessageFromTopic(myQueue);
            return Content(msgRead);
        }
    }
}