﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;
using Newtonsoft.Json;

namespace WebApplication1.Models
{
    public class PubSubManagement
    {
        public Topic CreateOrRetrieveTopic(string name)
        {
            TopicName topic = new TopicName("progforcloud63at", name);

            PublisherServiceApiClient client = PublisherServiceApiClient.Create();

            Topic myTopic = null;
            try
            {
                myTopic = client.GetTopic(topic);
            }
            catch (RpcException e)
            {
                if (e.Status.StatusCode == StatusCode.NotFound)
                    myTopic = client.CreateTopic(topic);

            }
            return myTopic;
            
        }


        public void PublishMessageToTopic(  string msg, Topic t)
        {
            

            try
            {
                PublisherServiceApiClient client = PublisherServiceApiClient.Create();

                List<PubsubMessage> myMessages = new List<PubsubMessage>();
                myMessages.Add(new PubsubMessage()
                {
                    Data = ByteString.CopyFromUtf8( msg)
                });

                client.Publish(t.TopicName, myMessages);
            }
            catch (Exception ex)
            {
                //log error
            }

        }


        public Subscription CreateOrRetrieveSubscription(string name, Topic t)
        {
            SubscriptionName subscription = new SubscriptionName("progforcloud63at", name);

            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();

            Subscription mySubscription = null;
            try
            {
                mySubscription = client.GetSubscription(subscription);
            }
            catch (RpcException e)
            {
                if (e.Status.StatusCode == StatusCode.NotFound)
                    mySubscription = client.CreateSubscription(
                        subscription, t.TopicName, null, 60);

            }
            return mySubscription;
        }



        public string PullMessageFromTopic(Topic t)
        {

            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();
           var mySubscription =  CreateOrRetrieveSubscription("SubscriptionMessageQueueMSD63A", t);

            //reading 1 message at a time
            PullResponse myMessage =  client.Pull(mySubscription.SubscriptionName, true, 1);

            ReceivedMessage msg = myMessage.ReceivedMessages.FirstOrDefault();
            if (msg == null) return "";
            else
            {
                string actualMessage = msg.Message.Data.ToStringUtf8();
                //if you are serializing the whole email, then you need to desrialize it using jsonconvert.deserialize


                // send email

                List<string> acksIds = new List<string>() { msg.AckId };
                client.Acknowledge(mySubscription.SubscriptionName, acksIds);
                return actualMessage;     
            }
        }



    }
}