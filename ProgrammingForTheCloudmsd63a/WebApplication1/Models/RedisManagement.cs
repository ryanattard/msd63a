﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using StackExchange.Redis;
using Newtonsoft.Json;

namespace WebApplication1.Models
{
    public class RedisManagement
    {

        ConnectionMultiplexer connection;
        public RedisManagement()
        {
            string strConnectionConfig = "redis-14991.c1.us-east1-2.gce.cloud.redislabs.com:14991,password=Kf9m4fNaKcHxRMKAU1mFLKGuPgNJsorM";

            connection = ConnectionMultiplexer.Connect(strConnectionConfig);
        }


        public bool SaveInRedis(List<Item> items)
        {
            var myDb = connection.GetDatabase();
            //to change from object to string
            string itemsStr = JsonConvert.SerializeObject(items);
            if (HasItemsChanged(items))
            {
                myDb.StringSet("items", itemsStr);
                return true;
            }
            else return false;
        }


        public List<Item> LoadFromRedis()
        {
            var myDb = connection.GetDatabase();
            string itemsStr = myDb.StringGet("items");
            if (string.IsNullOrEmpty(itemsStr)) return null;
            List<Item> items = JsonConvert.DeserializeObject<List<Item>>(itemsStr);
            return items;
        }

        private bool HasItemsChanged(List<Item> items)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();

            //hashing the data of the items we're about to store
            byte[] itemsAsBytes = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(items));
            string digestOfItemsToStore = Convert.ToBase64String(md5.ComputeHash(itemsAsBytes));

            //hashing the data of the items we already have in cache
            var itemsInRedis = LoadFromRedis();
            if (itemsInRedis == null) return true;
            byte[] itemsInRedisAsBytes = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(itemsInRedis));
            string digestOfItemsInRedis = Convert.ToBase64String(md5.ComputeHash(itemsInRedisAsBytes));

            //comparing whether they are equal or not
            if (digestOfItemsInRedis == digestOfItemsToStore) return false;
            else return true;
        }


    }
}