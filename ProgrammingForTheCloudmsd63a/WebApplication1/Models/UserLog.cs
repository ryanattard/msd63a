﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class UserLog
    {
        public string Email { get; set; }
        public DateTime LoggedIn { get; set; }
        public DateTime? LoggedOut { get; set; }

    }
}